import { GalleryScraper } from "./scrapers/galleries/index.mjs";
import { ImageScraper } from "./scrapers/images/index.mjs";
import { VideoScraper } from "./scrapers/videos/index.mjs";
import { downloadWithRetry } from "./util/download.mjs";

const scrapers = [new ImageScraper(), new GalleryScraper(), new VideoScraper()];

const urls = process.argv.slice(2);

console.log(urls);

for (const url of urls) {
  console.log(`Scraping ${url}`);

  const suitableScraper = scrapers.find((scraper) => scraper.canScrapeUrl(url));

  if (!suitableScraper) {
    console.error(`No suitable scraper found for url: ${url}`);
    continue;
  }

  const tasks = await suitableScraper.scrapeUrl(url);

  for (const task of tasks) {
    console.log(`Downloading ${task.url} to ${task.path}`);

    await downloadWithRetry(task.url, task.path, {
      onProgress: (percent) => {
        if (percent % 0.1 === 0) {
          console.log(percent);
        }
      },
    });
  }
}

process.exit(0);
