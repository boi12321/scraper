import { createWriteStream, existsSync, mkdirSync, unlinkSync } from "fs";
import { parse } from "path";
import https from "https";
import { IncomingMessage } from "http";

export interface IDownloadContext {
  onProgress?: (percent: number) => void;
}

export async function downloadFile(
  url: string,
  file: string,
  ctx?: IDownloadContext,
): Promise<void> {
  url = url.replace("http://", "https://");

  const response = await new Promise<IncomingMessage>((resolve) => {
    https.get(url, (response) => {
      resolve(response);
    });
  });

  const status = response.statusCode!;

  if (status > 399) {
    throw new Error(`Download failed with ${status}`);
  }

  if (status > 299) {
    const location = String(response.headers.location);
    console.error(`Download redirect: ${location}`);
    return downloadFile(location, file, ctx);
  }

  const { dir } = parse(file);

  if (!existsSync(dir)) {
    console.warn(`Creating folder ${dir}`);
    mkdirSync(dir, {
      recursive: true,
    });
  }

  if (existsSync(file)) {
    console.warn(`\t${url} already exists, skipping...`);
    return;
  }

  const writer = createWriteStream(file);

  const pipe = response.pipe(writer);

  const totalSize = parseInt(response.headers["content-length"] || "0");
  let loaded = 0;

  response.on("data", (data: Buffer) => {
    loaded += Buffer.byteLength(data);
    const percentFloat = loaded / totalSize;
    ctx?.onProgress?.(percentFloat);
  });

  await new Promise<void>((resolve, reject) => {
    pipe.on("finish", () => {
      resolve();
    });
    pipe.on("error", reject);
  });
}

function wait(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export async function downloadWithRetry(
  url: string,
  path: string,
  ctx?: IDownloadContext,
  maxRetries = 5,
): Promise<boolean> {
  let retryCount = 0;

  let linkDone = false;
  while (!linkDone) {
    try {
      await downloadFile(url, path, ctx);
      return true;
    } catch (error: any) {
      console.warn("Error downloading url:", error.message);

      try {
        // Clean up unfinished file
        unlinkSync(path);
      } catch (err) { }

      console.warn("Retrying url:", url);

      const waitMs = Math.pow(2, retryCount) * 1000;
      console.warn(`Waiting ${(waitMs / 1000).toFixed(1)} seconds`);
      await wait(waitMs);

      retryCount++;
      if (retryCount >= maxRetries) {
        console.error("Giving up on url:", url);
        return false;
      }
    }
  }

  return true;
}
