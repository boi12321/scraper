import axios from "axios";

export async function resolveRedirect(url: string): Promise<string> {
  while (true) {
    const res = await axios.get(url, {
      maxRedirects: 0,
      validateStatus: () => true,
    });
    if (res.status >= 300 && res.status < 400) {
      url = res.headers["location"] || res.headers["Location"];
    } else {
      return url;
    }
  }
}
