import Axios from "axios";
import cheerio from "cheerio";

type Headers = { [key: string]: string };

export type Options = Partial<{
  headers: Headers;
}>;

export async function createDomFromURL(url: string, opts: Options = {}) {
  const response = await Axios.get<string>(url, {
    headers: opts.headers || {},
  });
  const html = response.data;
  return cheerio.load(html);
}
