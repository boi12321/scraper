# porn-scrape-server

## Supported sites

### Videos

- hqporner.com
- fullporner.com
- porntrex.com

### Image galleries

- babesource.com
- blacksonblondes.com
- coedcherry.com
- europornstar.com
- pixhost.to
- pornpics.com
- porn-star.com
- sweet-pornstars.com
- thumbnailseries.com
- vipergirls.to

## Installation (using npm)

```
npm install
npm run build
```

Run:

```
npm start
```

## Installation (using yarn, recommended)

```
yarn
yarn build
```

Run:

```
yarn start
```

Visit http://localhost:4004 and add URLs
