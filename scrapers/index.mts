import { IScraper } from "./scraper.mjs";

export abstract class IScrapeRunner<T> {
  abstract scrapers: IScraper<T>[];

  findSuitableScraper(url: string): IScraper<T> | undefined {
    return this.scrapers.find((s) => s.canScrapeUrl(url));
  }

  canScrapeUrl(url: string): boolean {
    return !!this.findSuitableScraper(url);
  }

  abstract scrapeUrl(url: string): Promise<{
    url: string,
    path: string,
  }[]>;
}
