import { createDomFromURL } from "../../util/dom.mjs";
import { resolveRedirect } from "../../util/redirect.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

const blacklist = ["/smilies", "/expired"];

export class VipergirlsScraper extends IScraper<IGalleryScrapeResult> {
  domain = "vipergirls.to";

  async getImageLinks($: cheerio.Root): Promise<string[]> {
    const thumbUrls = (
      Array.from($("#postlist .content img"))
        .map((el) => $(el).attr("src"))
        .filter(Boolean) as string[]
    ).filter((url) => blacklist.every((x) => !url.includes(x)));

    const firstUrl = thumbUrls[0];
    if (firstUrl.includes("imx.to")) {
      const resolvedUrls = await Promise.all(thumbUrls.map((url) => resolveRedirect(url)));
      return resolvedUrls.map((url) => url.replace("/t/", "/i/"));
    } else if (firstUrl.includes("vipr.im")) {
      return thumbUrls.map((url) => url.replace("/th", "/i"));
    } else if (firstUrl.includes("pixhost.to")) {
      return thumbUrls.map((url) =>
        url.replace(/t(\d+).pixhost/, "img$1.pixhost").replace("thumbs", "images"),
      );
    } else if (firstUrl.includes("acidimg.cc")) {
      return thumbUrls.map((url) =>
        url.replace("acidimg", "i.acidimg").replace("upload/small", "i"),
      );
    } else if (firstUrl.includes("imagetwist")) {
      return thumbUrls.map((url) => url.replace("/th", "/i"));
    }
    throw new Error("Unsupported URL " + firstUrl);
  }

  async scrapeUrl(url: string) {
    const galleryId = url
      .match(/threads\/\d+/)![0]
      .split("/")
      .pop()!;

    const dom = await createDomFromURL(url);
    const links = await this.getImageLinks(dom);

    return {
      gallery: `vipergirls-${galleryId}`,
      links: links.filter(Boolean),
    };
  }
}
