import { IGalleryScrapeResult } from "./interface.mjs";
import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";

export class BabesourceScraper extends IScraper<IGalleryScrapeResult> {
  domain = "babesource.com";

  getImageLinks($: cheerio.Root): string[] {
    return Array.from($(".box-massage__card-link")).map((el) => {
      return $(el).attr("href")!;
    });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!.replace(".html", "");

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(dom);

    return {
      gallery,
      links,
    };
  }
}
