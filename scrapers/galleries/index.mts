import { PornStarScraper } from "./pornstar.mjs";
import { BabesourceScraper } from "./babesource.mjs";
import { CoedcherryScraper } from "./coedcherry.mjs";
import { PornpicsScraper } from "./pornpics.mjs";
import { SweetPornstarsScraper } from "./sweet-pornstars.mjs";
import { EuropornstarScraper } from "./europornstar.mjs";
import { ThumbnailSeriesScraper } from "./thumbnailseries.mjs";
import { PixhostScraper } from "./pixhost.mjs";
import { IScrapeRunner } from "../index.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";
import { extname, resolve } from "path";
import { DogfartScraper } from "./blacksonblondes.mjs";
import { VipergirlsScraper } from "./vipergirls.mjs";

export class GalleryScraper extends IScrapeRunner<IGalleryScrapeResult> {
  scrapers = [
    new BabesourceScraper(),
    new PornStarScraper(),
    new CoedcherryScraper(),
    new PornpicsScraper(),
    new SweetPornstarsScraper(),
    new EuropornstarScraper(),
    new ThumbnailSeriesScraper(),
    new PixhostScraper(),
    new DogfartScraper(),
    new VipergirlsScraper(),
  ];

  async scrapeUrl(url: string) {
    const scraper = this.findSuitableScraper(url);

    if (!scraper) {
      return [];
    }

    const { gallery, links } = await scraper.scrapeUrl(url);

    return links.filter(Boolean).map((url, i) => {
      const num = (i + 1).toString().padStart(3, "0");
      const ext = extname(url).split("?")[0];
      const path = resolve(process.env.IMAGE_FOLDER!, gallery, `${num}${ext}`);

      return {
        url,
        path,
      }
    });
  }
}
