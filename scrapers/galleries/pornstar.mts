import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class PornStarScraper extends IScraper<IGalleryScrapeResult> {
  domain = "porn-star.com";

  getImageLinks(gallery: string, $: cheerio.Root): string[] {
    return Array.from($(".thumbnails-gallery img"))
      .map((el) => {
        return $(el).attr("src")!;
      })
      .map((imageSrc) => {
        console.log(imageSrc);
        console.log(`https://porn-star.com/${gallery}/${imageSrc.replace("thumbs/", "")}`);
        return `https://porn-star.com/${gallery}/${imageSrc.replace("thumbs/", "")}`;
      });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.replace("index.html", "").split("/").filter(Boolean);
    const gallery = urlSegments[urlSegments.length - 1];

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(gallery, dom);

    return {
      gallery,
      links,
    };
  }
}
