import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class SweetPornstarsScraper extends IScraper<IGalleryScrapeResult> {
  domain = "sweet-pornstars.com";

  getImageLinks($: cheerio.Root): string[] {
    return Array.from($(".gallery .card-image a"))
      .map((el) => {
        return $(el).attr("href")!;
      })
      .map((url) => `https://sweet-pornstars.com${url}`);
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments[urlSegments.length - 1];

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(dom);

    return {
      gallery,
      links,
    };
  }
}
