import { IGalleryScrapeResult } from "./interface.mjs";
import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";

async function chunkAsync<T>(
  arr: T[],
  n: number,
  func: (chunk: T[]) => Promise<void>,
): Promise<void> {
  for (let i = 0; i < arr.length; i += n) {
    await func(arr.slice(i, i + n));
  }
}

export class DogfartScraper extends IScraper<IGalleryScrapeResult> {
  domain = "dogfartnetwork.com";

  private async getRawImageLinks($: cheerio.Root): Promise<string[]> {
    console.error("Getting raw links");

    const lastPageUrl = $(".scenes-pagination .pagination li a").last().attr("href")!;
    const canonical = this.getCanonicalUrl($);

    const lastPage$ = await createDomFromURL(`${canonical}${lastPageUrl}`);

    const lastImageUrl = lastPage$(".preview-image-container a").last().attr("href")!;

    const lastNumber = Number((lastImageUrl.match(/\d+.jpg/) || [])[0]!.replace(".jpg", ""));

    let images: string[] = [];

    for (let i = 1; i <= lastNumber; i++) {
      const id = i.toString().padStart(3, "0");
      images.push(`http://${this.domain}${lastImageUrl.replace(/\d\d\d/, id)}`);
    }

    return images;
  }

  private getCanonicalUrl($: cheerio.Root): string {
    const canonical = Array.from($(`link[rel="canonical"]`))[0];
    return $(canonical).attr("href")!;
  }

  private getGalleryId($: cheerio.Root): string {
    const href = this.getCanonicalUrl($);
    const urlSegments = href.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!;
    const site = urlSegments.pop()!;
    return `${site}_${gallery}`;
  }

  private async resolveImageUrl(url: string): Promise<string> {
    const $ = await createDomFromURL(url);
    const imageUrl = $(".scenes-module img").first().attr("src")!;
    return imageUrl;
  }

  async scrapeUrl(url: string) {
    const dom = await createDomFromURL(url);

    const rawLinks = await this.getRawImageLinks(dom);
    console.error(`Got ${rawLinks.length} URLs`);

    const imageLinks: string[] = [];

    await chunkAsync(rawLinks, 100, async (links) => {
      const chunk = await Promise.all(links.map((link) => this.resolveImageUrl(link)));
      imageLinks.push(...chunk);
      console.error(`Resolved ${imageLinks.length}/${rawLinks.length} URLs`);
    });

    const gallery = this.getGalleryId(dom);

    return {
      gallery,
      links: imageLinks,
    };
  }
}
