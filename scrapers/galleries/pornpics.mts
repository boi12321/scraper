import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class PornpicsScraper extends IScraper<IGalleryScrapeResult> {
  domain = "pornpics.com";

  getImageLinks($: cheerio.Root): string[] {
    return Array.from($("#tiles .rel-link")).map((el) => {
      return $(el).attr("href")!;
    });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!;

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(dom);

    return {
      gallery,
      links,
    };
  }
}
