import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class PixhostScraper extends IScraper<IGalleryScrapeResult> {
  domain = "pixhost.to";

  getImageLinks($: cheerio.Root): string[] {
    return Array.from($(".images a")).map((el) => {
      return $(el).attr("src")!;
    });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!;

    const dom = await createDomFromURL(url);
    const imagePages = this.getImageLinks(dom);

    const links: string[] = [];

    for (const url of imagePages) {
      const $ = await createDomFromURL(url);
      const image = Array.from($("#image"))[0];
      links.push($(image).attr("src")!);
    }

    return {
      gallery,
      links,
    };
  }
}
