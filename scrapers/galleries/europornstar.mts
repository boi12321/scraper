import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class EuropornstarScraper extends IScraper<IGalleryScrapeResult> {
  domain = "europornstar.com";

  getImageLinks($: cheerio.Root, url: string): string[] {
    return Array.from($(".thumbs .th a")).map((el) => {
      return url + $(el).attr("href");
    });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!.replace(".html", "");

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(dom, url);

    return {
      gallery,
      links,
    };
  }
}
