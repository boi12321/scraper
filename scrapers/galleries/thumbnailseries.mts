import { createDomFromURL } from "../../util/dom.mjs";
import { IScraper } from "../scraper.mjs";
import { IGalleryScrapeResult } from "./interface.mjs";

export class ThumbnailSeriesScraper extends IScraper<IGalleryScrapeResult> {
  domain = "thumbnailseries.com";

  getImageLinks($: cheerio.Root): string[] {
    return Array.from($("#light-gallery > a")).map((el) => {
      return `https://www.${this.domain}${$(el).attr("href")}`;
    });
  }

  async scrapeUrl(url: string) {
    const urlSegments = url.split("/").filter(Boolean);
    const gallery = urlSegments.pop()!;

    const dom = await createDomFromURL(url);
    const links = this.getImageLinks(dom);

    return {
      gallery,
      links,
    };
  }
}
