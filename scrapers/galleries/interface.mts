export interface IGalleryScrapeResult {
  gallery: string;
  links: string[];
}
