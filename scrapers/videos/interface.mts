export interface IVideoScrapeResult {
  id: string;
  name: string;
  fileUrl: string;
  size: number;
}
