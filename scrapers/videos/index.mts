import { resolve } from "path";
import { IScrapeRunner } from "../index.mjs";
import { FullPornerScraper } from "./fullporner.mjs";
import { HQPornerScraper } from "./hqporner.mjs";
import { IVideoScrapeResult } from "./interface.mjs";
import { PorntrexScraper } from "./porntrex.mjs";

export class VideoScraper extends IScrapeRunner<IVideoScrapeResult> {
  scrapers = [new HQPornerScraper(), new PorntrexScraper(), new FullPornerScraper()];

  async scrapeUrl(url: string) {
    const scraper = this.findSuitableScraper(url);

    if (!scraper) {
      return [];
    }

    const { name, fileUrl } = await scraper.scrapeUrl(url);
    const quality = process.env.VIDEO_QUALITY || "1080";
    const path = resolve(process.env.VIDEO_FOLDER!, `${name}-${quality}p.mp4`);

    return [
      {
        url: fileUrl,
        path,
      }
    ];
  }
}
