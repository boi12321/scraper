import { createDomFromURL } from "../../util/dom.mjs";
import { IVideoScrapeResult } from "./interface.mjs";
import Axios from "axios";
import { IScraper } from "../scraper.mjs";

export class FullPornerScraper extends IScraper<IVideoScrapeResult> {
  domain = "fullporner.com";

  async scrapeUrl(url: string) {
    const $ = await createDomFromURL(url);

    const iframe = $(".single-video iframe").toArray()[0];

    if (!iframe) {
      console.error("Iframe");
      process.exit(1);
    }

    const iframeUrl = $(iframe).attr("src");

    if (!iframeUrl) {
      console.error("Iframe URL not found");
      process.exit(1);
    }

    const videoName = $(".single-video-title h2").text().trim();
    console.error(`Found video: ${videoName}`);
    const splits = iframeUrl.split("/").filter(Boolean);
    const videoId = splits[splits.length - 1];
    console.error(`ID: ${videoId}`);

    const $iframe = await createDomFromURL(`https:${iframeUrl}`);

    const quality = process.env.VIDEO_QUALITY || "1080";

    const qualityRegex = new RegExp(`\/\/[a-zA-Z\/0-9.]+${quality}.mp4`);
    const xmlStr = $iframe.xml();
    const matches = xmlStr.match(qualityRegex);

    if (matches && matches.length) {
      const matchedStr = matches[0];
      const cleanUrl = `https:${matchedStr}`;

      const res = await Axios.head(cleanUrl);
      const size = parseInt(res.headers["content-length"]);

      return {
        id: videoId,
        name: videoName,
        fileUrl: cleanUrl,
        size,
      };
    } else {
      console.error(xmlStr);
      throw new Error(`Quality ${quality}p not found for ${url}`);
    }
  }
}
