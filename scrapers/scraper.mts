export abstract class IScraper<T> {
  protected abstract domain: string;

  abstract scrapeUrl(url: string): Promise<T>;

  canScrapeUrl(url: string): boolean {
    return url.includes(this.domain);
  }
}
